<?php
//login
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('dashboard','DashboardController@index')->name('dashboard');

Route::get('/home', 'HomeController@index')->name('home');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::resetPassword();
// Email Verification Routes...
Route::emailVerification();


// creacion de rutas para usuarios
Route::get('/Usuario/Altas','Decse\UsuariosController@vistaAltaUsuario');
Route::post('/Usuario/Registro','Decse\UsuariosController@Alta_usuario');

// ver datos de actualizar
Route::get('/Usuario/Editar/{id}','Decse\UsuariosController@ver_usuario');
Route::post('/Usuario/Actualizar','Decse\UsuariosController@actualizar_usuario');

// eliminar
Route::get('/Usuario/Eliminar/{id}','Decse\UsuariosController@ver_usuario_eli');
Route::post('/Usuario/Baja','Decse\UsuariosController@eliminar_usuario');
//ver lista de los usuarios
Route::get('/listaempleados','Decse\UsuariosController@ver_tabla');
//creacion de rutas para clientes
Route::get('/Cliente/Altas','Decse\ClientesController@VistaAgregarCliente');
Route::post('/Cliente/Registro','Decse\ClientesController@AgregarClientes');

// ver datos de actualizar
Route::get('/Cliente/Editar/{idcliente}','Decse\ClientesController@ver_cliente');
Route::post('/Cliente/Actualizar','Decse\ClientesController@actualizar_cliente');

// eliminar
Route::get('/Cliente/Eliminar/{idcliente}','Decse\ClientesController@ver_cliente_eli');
Route::post('/Cliente/Baja','Decse\ClientesController@eliminar_cliente');
//lista de los clientes
Route::get('/listaclientes','Decse\ClientesController@VerTablaClientes');
//creacion de rutas para vehiculos
Route::get('/Vehiculo/Altas','Decse\VehiculosController@VistaAgregarVehiculos');
Route::post('/Vehiculo/Registro','Decse\VehiculosController@AgregarVehiculos');

// ver datos de actualizar
Route::get('/Vehiculo/Editar/{idunidad}','Decse\VehiculosController@ver_Vehiculos');
Route::post('/Vehiculo/Actualizar','Decse\VehiculosController@actualizar_Vehiculos');

// eliminar
Route::get('/Vehiculo/Eliminar/{idunidad}','Decse\VehiculosController@ver_Vehiculos_eli');
Route::post('/Vehiculo/Baja','Decse\VehiculosController@eliminar_Vehiculos');

//lista de los vehiculos
Route::get('/listaV','Decse\VehiculosController@VerTablaVehiculos');
//creacion de rutas para servicios
Route::get('/Servicios/Altas','Decse\ServiciosController@VistaAgregarServicio');
Route::post('/Servicios/Registro','Decse\ServiciosController@AgregarServicio');

//Route::get('/Servicios/Altas','Decse\ServiciosController@VistaAgregarServicio');

// ver datos de actualizar
Route::get('/Servicios/Editar/{idservicio}','Decse\ServiciosController@ver_servicio');
Route::post('/Servicios/Actualizar','Decse\ServiciosController@actualizar_servicio');

// eliminar
Route::get('/Servicios/Eliminar/{idservicio}','Decse\ServiciosController@ver_servicio_eli');
Route::post('/Servicios/Baja','Decse\ServiciosController@eliminar_servicio');
//lista de los servicios
Route::get('/listaS','Decse\ServiciosController@VerTablaServicios');

Route::get('/listaServiciosAsignados','Decse\ServiciosController@VerTablaServicios');

// eliminar por bandera
Route::get('/Usuario/Eliminar_bandera/{id}','Decse\UsuariosController@ver_usuario_bandera');
Route::post('/Usuario/Baja_bandera','Decse\UsuariosController@eliminar_usuario_bandera');
//relacionar los datos del cliente con los datos del servicio que se solicita
// creacion de rutas para usuarios
Route::get('/Solicitud/Altas','Decse\SolicitudController@VistaAgregarSolicitud');
Route::post('/Solicitud/Registro','Decse\SolicitudController@AgregarSolicitud');

// ver datos de actualizar
Route::get('/Solicitud/Editar/{idsolicitud}','Decse\SolicitudController@ver_solicitud');
Route::post('/Solicitud/Actualizar','Decse\SolicitudController@actualizar_solicitud');

// eliminar
Route::get('/Solicitud/Eliminar/{ididsolicitud}','Decse\SolicitudController@ver_solicitud_eli');
Route::post('/Solicitud/Baja','Decse\SolicitudController@eliminar_solicitud');
//ver lista de los usuarios
Route::get('/listaSolicitud','Decse\SolicitudController@VerTablaSolicitudes');