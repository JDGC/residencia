$(function () {
    $('#select-producto').on('change', funcion1);
    $('#cantidad').on('change', subtotal);
    $('#descuento').on('change', descuento);
});

function funcion1()
{
  var tipo_producto = document.getElementById("select-producto").value;
  $.get('/Productos/costo/' + tipo_producto + '',
  function (data) {
           $('#costo_producto').val(data['costo']);
    //console.log(data['costo']);
  });
}

function subtotal()
{
  var costo_unitario = document.getElementById("costo_producto").value;
  var cantidad_salida = document.getElementById("cantidad").value;
  var subtotal = costo_unitario * cantidad_salida;

  $('#subtotal').val(subtotal);
}

function descuento()
{
  var subtotal = document.getElementById("subtotal").value;
  var descuento = document.getElementById("descuento").value;
  var resta_descuento = subtotal * descuento;

  var total = subtotal - resta_descuento;

   $('#total').val(total);
}
