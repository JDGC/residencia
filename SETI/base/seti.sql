-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-02-2021 a las 06:20:30
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `seti`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nombrec` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `apellidop` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `apellidom` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `telefono` varchar(20) COLLATE latin1_general_cs DEFAULT NULL,
  `email` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `empresa` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `calle` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `colonia` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `municipio` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `estado` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `codigopostal` int(11) DEFAULT NULL,
  `referencia` varchar(200) COLLATE latin1_general_cs DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombrec`, `apellidop`, `apellidom`, `telefono`, `email`, `empresa`, `calle`, `numero`, `colonia`, `municipio`, `estado`, `codigopostal`, `referencia`) VALUES
(1, 'Maximina Crisanta', 'Ramirez', 'calvo', '9512584788', 'maxi@gmail.com', 'comex', 'moctezuma', 315, 'samaritana', 'Santa Maria Atzompa', 'oaxaca de juarez', 85858, 'porton azul'),
(2, 'Jose Daniel', 'Calvo', 'Lopez', '951 14 78 58', 'chepeguas@gmail.com', 'sfsfdsfsdf', 'Moctezuma', 15, 'samaritana', 'Santa Maria Atzompa', 'oaxaca', 71220, 'porton azul');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `idservicio` int(11) NOT NULL,
  `tiposervicio` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `descripcion` varchar(250) COLLATE latin1_general_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`idservicio`, `tiposervicio`, `fecha`, `hora`, `descripcion`) VALUES
(1, 'instalacion de equip', '2021-02-04', '03:50:00', 'meter cableado para videovigilancia'),
(2, 'instalacion de equip', '2021-02-04', '22:40:00', 'meter cableado para videovigilancia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudservicio`
--

CREATE TABLE `solicitudservicio` (
  `idsolicitud` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `nservicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Volcado de datos para la tabla `solicitudservicio`
--

INSERT INTO `solicitudservicio` (`idsolicitud`, `idservicio`, `idcliente`, `nservicio`) VALUES
(2, 1, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `apellidop` varchar(50) DEFAULT NULL,
  `apellidom` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `calle` varchar(50) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `municipio` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `codigopostal` int(11) DEFAULT NULL,
  `fechanacimiento` date NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `tipo_usuario` varchar(50) DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `activo` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `apellidop`, `apellidom`, `telefono`, `calle`, `numero`, `colonia`, `municipio`, `estado`, `codigopostal`, `fechanacimiento`, `email`, `password`, `tipo_usuario`, `fecha_alta`, `activo`) VALUES
(6, 'daniel', 'diaz', 'calvo', '951 14 78 58', 'moctezuma', 15, 'samaritana', 'asdasd', 'oaxaca', 71220, '2021-02-02', 'daniel@gmail.com', '$2y$10$0gc3jKhtffrVRvtif.VuNO3Hz1ExJJuYRJi9efwHiwvtZM98cAKPa', '1', '2021-02-02', NULL),
(8, '14161242', 'gutierrez', 'alcantara', '951 14 78 58', 'moctezuma', 15, 'samaritana', 'santa maria atzompa', 'oaxaca de juarez', 71220, '2021-02-02', '14161242@gmail.com', '$2y$10$qFd8UiccaWgsN6debhoqzuVsHOhz9Xyaeoh1GrqjYocD4tz4ofiqq', '0', '2021-02-02', NULL),
(9, 'sdsdadsad', 'sdasdasdasd', 'ssadasdasd', '951147858', 'ssdadsdasd', 55, 'asdasdsd', 'asdasdsad', 'asdasdasdasd', 71220, '2021-02-02', 'prueba@gmail.com', '$2y$10$lpsoR4SqQvxJ1OxGU0yGPOv6yeD.tx0kWhAx66jmLvHBWDc8VmZTm', '0', '2021-02-09', NULL),
(10, 'jose', 'jose', 'jose', '9511474747', 'jose', 23, 'jose', 'jose', 'jose', 71225, '2021-02-02', 'jose@gmail.com', '$2y$10$TC.XWHDJV6sI6oQMEcf.zenXLs2nE7U8AHToOvnIVuVyUMSNdjVgO', '1', '2021-02-02', NULL),
(11, 'sadasasdasd', 'asdasdasdasd', 'asdasdsada', '951320505', 'asdsadasd', 15, 'asdasdasd', 'asdsdasd', 'asdasd', 71220, '2021-02-02', 'loco@gmail.com', '$2y$10$ONpdeNaCRk0k.TR9UTsEOO984pc6DyYSHoAgTzLir5T96lpKoazsm', '0', '2021-02-02', NULL),
(14, 'dsdsadsdasd', 'asdasdasd', 'asdasdasd', '9512586944', 'Moctezuma', 315, 'samaritana', 'santa maria atzompa', 'oaxaca de juarez', 71220, '2021-02-02', 'dano@gmail.com', '$2y$10$6uRUgnPaEn1pshsLeXY0pO.mEz8SBMY4QNFBC8s7eOE24umvVdKx2', '1', '2021-02-02', NULL),
(16, 'juan diego', 'diaz', 'alcantara', '9511477474', 'betania', 1, 'samaritana', 'santa maria atzompa', 'pruebs', 71220, '2021-01-31', 'juandiego@gmail.com', '$2y$10$pDRrGribrH5R/UFgWJEFzuw/cgbvT1RM3zs.47gSce.e38SdcluJu', '4', '2021-01-01', NULL),
(17, 'Jose Daniel', 'Calvo', 'alcantara', '951350608', 'moctezuma', 44, 'san sebastian', 'centro oaxaca', 'oaxaca', 71220, '2020-01-01', 'chepeguas@gmail.com', '$2y$10$36rowe9.hPSKYzoF0AhkZe4nCnn4trNt1CdmxtwbUrU67yglqwi6K', '1', '2021-01-01', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `idunidad` int(11) NOT NULL,
  `tipotransporte` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `modelo` int(11) DEFAULT NULL,
  `estado` varchar(50) COLLATE latin1_general_cs DEFAULT NULL,
  `fechaservicio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`idunidad`, `tipotransporte`, `modelo`, `estado`, `fechaservicio`) VALUES
(2, 'Camioneta', 2005, 'oaxaca', '2021-02-03');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`idservicio`);

--
-- Indices de la tabla `solicitudservicio`
--
ALTER TABLE `solicitudservicio`
  ADD PRIMARY KEY (`idsolicitud`),
  ADD KEY `idservicio` (`idservicio`,`idcliente`),
  ADD KEY `idcliente` (`idcliente`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`idunidad`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `idservicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `solicitudservicio`
--
ALTER TABLE `solicitudservicio`
  MODIFY `idsolicitud` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `idunidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `solicitudservicio`
--
ALTER TABLE `solicitudservicio`
  ADD CONSTRAINT `solicitudservicio_ibfk_1` FOREIGN KEY (`idservicio`) REFERENCES `servicios` (`idservicio`) ON DELETE CASCADE,
  ADD CONSTRAINT `solicitudservicio_ibfk_2` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
