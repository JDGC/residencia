<?php

namespace App\Models\Decse;

use Illuminate\Database\Eloquent\Model;

class UnidadTrasnporteModel extends Model
{
  protected $table = 'vehiculos';
  protected $primarykey = 'idunidad';
  public $timestamps = false;

  protected $fillable = [
    'idunidad','tipotransporte','modelo','estado','fechaservicio'
 	];
}

?>
