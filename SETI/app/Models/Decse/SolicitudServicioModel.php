<?php

namespace App\Models\Decse;

use Illuminate\Database\Eloquent\Model;

class SolicitudServicioModel extends Model
{
  protected $table = 'solicitudservicio';
  protected $primarykey = 'idsolicitud';
  public $timestamps = false;

  protected $fillable = [
    'idsolicitud','idservicio','idcliente','nservicio'
  ];
}

?>
