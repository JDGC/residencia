<?php

namespace App\Models\Decse;

use Illuminate\Database\Eloquent\Model;

class UsuarioModel extends Model
{
  protected $table = 'usuarios';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
    'id','usuario','apellidop','apellidom','telefono','calle','numero','colonia','municipio','estado','codigopostal','fechanacimiento','email','password','tipo_usuario','fecha_alta',
  ];
}


?>
