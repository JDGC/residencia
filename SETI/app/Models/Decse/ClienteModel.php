<?php

namespace App\Models\Decse;

use Illuminate\Database\Eloquent\Model;

class ClienteModel extends Model
{
  protected $table = 'cliente';
  protected $primarykey = 'idcliente';
  public $timestamps = false;

  protected $fillable = [
    'idcliente','nombrec','apellidop','apellidom','telefono','email','empresa','calle','numero','colonia','municipio','estado','codigopostal','referencia'
  ];
}

?>
