<?php

namespace App\Models\Decse;

use Illuminate\Database\Eloquent\Model;

class ServiciosModel extends Model
{
  protected $table = 'servicios';
  protected $primarykey = 'idservicio';
  public $timestamps = false;

  protected $fillable = [
    'idservicio','tiposervicio','fecha','hora','descripcion'
  ];
}
return redirect()->to('/listaServicios'); 

?>
