<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/Principal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
      return view('Users/login');
      }

    public function login()
    {
       $acceso = $this->validate(request(),['email'=>'required|string'
       ,'password'=>'required|string']);

       if(Auth::attempt($acceso))
       {
          return redirect()->route('dashboard');;
       }
       else
       {
          ?>
              <script type="text/javascript">
                          alert("Credenciales incorrectas, intente de nuevo.");
                          document.location.href = "login";
              </script>

          <?php
       }
    }
    public function logout(){
      Auth::logout();

      return redirect('/');
  }
}
