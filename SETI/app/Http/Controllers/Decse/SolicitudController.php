<?php
namespace App\Http\Controllers\Decse;
//se declara el controlador
use App\Http\Controllers\Controller;
use App\Categoria;
// declaracion del uso del controlador

// habilita request
use Illuminate\Http\Request;
/*
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Storage;
*/
// se declara modelos (bd)
use App\Models\Decse\UsuarioModel;
use App\Models\Decse\SolicitudServicioModel;
use App\Models\Decse\ClienteModel;
use App\Models\Decse\ServiciosModel;
use App\Models\Decse\UnidadTrasnporteModel;
class SolicitudController extends Controller
{

    /*public function nuevo_servicio($idcliente)
  	{
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = ClienteModel::
     select('idcliente','nombrec','apellidop','apellidom','telefono','email','empresa','calle','numero','colonia','municipio','estado','codigopostal','referencia')->where('idcliente',$idcliente)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users/agregarServicios')
     ->with('cliente',$ver_usuario1);
  }

  public function AgregarServicio(Request $request)
  {
    $idcliente = $request->idcliente;
    $nombrec = $request->nombrec;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
    $email = $request->email;
    $empresa = $request->empresa;
    $calle = $request->calle;
    $numero = $request->numero;
    $colonia = $request->colonia;
    $municipio = $request->municipio;
    $estado = $request->estado;
    $codigopostal = $request->codigopostal;
    $referencia = $request->referencia;

    $servicio = ClienteModel::where('idcliente',$idcliente)->first();

    	$idservicio = $servicio->idcliente;
        $tiposervicio = $request->tiposervicio;
        $fecha = $request->fecha;
        $hora = $request->hora;
        $descripcion = $request->descripcion;

        ServiciosModel::create([
            'idservicio'=>$idservicio,
            'idcliente' =>$idcliente,
            'tiposervicio'=>$tiposervicio,
            'fecha'=>$fecha,
            'hora'=>$hora,
            'descripcion'=>$descripcion,
    ]);

    return redirect()->to('/listaS');
  }*/
  public function VistaAgregarSolicitud() {
    $datosc = ClienteModel::pluck('nombrec','idcliente');
    //$datosS = ServiciosModel::pluck('tiposervicio','idservicio');
    return view('Solicitud/agregarSolicitud')->with('datosc');
    }
    public function AgregarSolicitud(Request $request){

    $idservicio = $request->idservicio;
    $idcliente = $request->idcliente;
    $nservicio = $request->nservicio;

//insercion
    SolicitudServicioModel::create(['idservicio' => $idservicio, 'idcliente' => $idcliente, 'nservicio' => $nservicio]);

return redirect()->to('/listaSolicitud');
    }


    public function ver_solicitud($idsolicitud)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_solicitud = SolicitudServicioModel::
     select('idsolicitud','idservicio','idcliente','nservicio')->where('idsolicitud',$idsolicitud)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Solicitud\actualizarSolicitud')
     ->with('solicitud',$ver_solicitud);
  }

  public function actualizar_solicitud(Request $request)
  {
    $idsolicitud = $request->idsolicitud;
    $idservicio = $request->idservicio;
    $idcliente = $request->idcliente;
    $nservicio = $request->nservicio;


    $ver_servicio = SolicitudServicioModel::where('idsolicitud',$idsolicitud)
    ->update([
      'idservicio' => $idservicio, 'idcliente' => $idcliente, 'nservicio' => $nservicio
    ]);

    return redirect()->to('/listaSolicitud');
  }

  public function ver_solicitud_eli($idsolicitud)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_solicitud = SolicitudServicioModel::
     select('idsolicitud','idservicio','idcliente','nservicio')->where('idsolicitud',$idsolicitud)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Solicitud\eliminarSolicitud')
     ->with('solicitud',$ver_solicitud);
  }

  public function eliminar_solicitud(Request $request)
  {
    $idsolicitud = $request->idsolicitud;
    $idservicio = $request->idservicio;
    $idcliente = $request->idcliente;
    $nservicio = $request->nservicio;


    $ver_solicitud = SolicitudServicioModel::where('idsolicitud',$idsolicitud)
    ->delete();

    return redirect()->to('/listaSolicitud');
  }

  public function VerTablaSolicitudes() {
    $tablaSolicitudes = SolicitudServicioModel::all();//->where('activo','1');
    return view('Solicitud/ListarSolicitud')->with('soliciud', $tablaSolicitudes);

	}
}