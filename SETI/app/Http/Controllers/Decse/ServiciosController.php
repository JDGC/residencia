<?php
namespace App\Http\Controllers\Decse;
//se declara el controlador
use App\Http\Controllers\Controller;
use App\Categoria;
// declaracion del uso del controlador

// habilita request
use Illuminate\Http\Request;
/*
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Storage;
*/
// se declara modelos (bd)
use App\Models\Decse\UsuarioModel;
use App\Models\Decse\SolicitarServicioModel;
use App\Models\Decse\ClienteModel;
use App\Models\Decse\ServiciosModel;
use App\Models\Decse\UnidadTrasnporteModel;
class ServiciosController extends Controller
{

    /*public function nuevo_servicio($idcliente)
  	{
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = ClienteModel::
     select('idcliente','nombrec','apellidop','apellidom','telefono','email','empresa','calle','numero','colonia','municipio','estado','codigopostal','referencia')->where('idcliente',$idcliente)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users/agregarServicios')
     ->with('cliente',$ver_usuario1);
  }

  public function AgregarServicio(Request $request)
  {
    $idcliente = $request->idcliente;
    $nombrec = $request->nombrec;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
    $email = $request->email;
    $empresa = $request->empresa;
    $calle = $request->calle;
    $numero = $request->numero;
    $colonia = $request->colonia;
    $municipio = $request->municipio;
    $estado = $request->estado;
    $codigopostal = $request->codigopostal;
    $referencia = $request->referencia;

    $servicio = ClienteModel::where('idcliente',$idcliente)->first();

    	$idservicio = $servicio->idcliente;
        $tiposervicio = $request->tiposervicio;
        $fecha = $request->fecha;
        $hora = $request->hora;
        $descripcion = $request->descripcion;

        ServiciosModel::create([
            'idservicio'=>$idservicio,
            'idcliente' =>$idcliente,
            'tiposervicio'=>$tiposervicio,
            'fecha'=>$fecha,
            'hora'=>$hora,
            'descripcion'=>$descripcion,
    ]);

    return redirect()->to('/listaS');
  }*/
  public function VistaAgregarServicio() {
      return view('Users/agregarServicios');
    }
    public function AgregarServicio(Request $request){

    $tiposervicio = $request->tiposervicio;
    $fecha = $request->fecha;
    $hora = $request->hora;
    $descripcion = $request->descripcion;

//insercion
    ServiciosModel::create(['tiposervicio' => $tiposervicio, 'fecha' => $fecha, 'hora' => $hora,'descripcion' => $descripcion]);

return redirect()->to('/listaSolicitud');
    }


    public function ver_servicio($idservicio)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_servicio = ServiciosModel::
     select('idservicio','tiposervicio','fecha','hora','descripcion')->where('idservicio',$idservicio)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\actualizarServicio')
     ->with('servicio',$ver_servicio);
  }

  public function actualizar_servicio(Request $request)
  {
    $idservicio = $request->idservicio;
    $tiposervicio = $request->tiposervicio;
    $fecha = $request->fecha;
    $hora = $request->hora;
    $descripcion = $request->descripcion;


    $ver_servicio = ServiciosModel::where('idservicio',$idservicio)
    ->update([
      'tiposervicio' => $tiposervicio, 'fecha' => $fecha, 'hora' => $hora,'descripcion' => $descripcion
    ]);

    return redirect()->to('/listaSolicitud');
  }

  public function ver_servicio_eli($idservicio)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_servicio = ServiciosModel::
     select('idservicio','tiposervicio','fecha','hora','descripcion')->where('idservicio',$idservicio)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\eliminarServicio')
     ->with('cliente',$ver_servicio);
  }

  public function eliminar_servicio(Request $request)
  {
    $idservicio = $request->idservicio;
    $tiposervicio = $request->tiposervicio;
    $fecha = $request->fecha;
    $hora = $request->hora;
    $descripcion = $request->descripcion;


    $ver_servicio = ServiciosModel::where('idservicio',$idservicio)
    ->delete();

    return redirect()->to('/listaSolicitud');
  }

  public function VerTablaServicios() {
    $tablaServicios3 = ServiciosModel::all();//->where('activo','1');
    $listaCC = [''=>''] + ClienteModel::pluck('nombrec','idcliente')->all();
    $listaEE = [''=>''] + UsuarioModel::pluck('usuario','id')->all();  
      $listaVV = [''=>''] + UnidadTrasnporteModel::pluck('tipotransporte','idunidad')->all(); 
      return view('Users/ListarServicios', compact('tablaServicios3','listaCC','listaEE','listaVV'));

	}
}