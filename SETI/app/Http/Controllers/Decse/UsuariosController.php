<?php
namespace App\Http\Controllers\Decse;
//se declara el controlador
use App\Http\Controllers\Controller;
// declaracion del uso del controlador

// habilita request
use Illuminate\Http\Request;

/*use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Storage;*/

// se declara modelos (bd)
use App\Models\Decse\UsuarioModel;
use App\User;

class UsuariosController extends Controller
{
  public function _construct()
  {
  }

  public function vistaAltaUsuario()
  {
     return view('Users/agregarUsuarios');
  }

  public function Alta_usuario(Request $request)
  {
     $usuario = $request->usuario;
     $apellidop = $request->apellidop;
     $apellidom = $request->apellidom;
     $telefono = $request->telefono;
     $calle = $request->calle;
     $numero = $request->numero;
     $colonia = $request->colonia;
     $municipio = $request->municipio;
     $estado = $request->estado;
     $codigopostal = $request->codigopostal;
     $fechanacimiento = $request->fechanacimiento;
     $email = $request->email;
     $password = bcrypt($request->password);
     $tipo_usuario = $request->tipo_usuario;
     $fecha_alta = $request->fecha_registro;

     User::create(['usuario' => $usuario,'apellidop' => $apellidop,'apellidom' => $apellidom,'telefono' => $telefono,'calle' => $calle,'numero' => $numero,'colonia' => $colonia,'municipio' => $municipio,'estado' => $estado,'codigopostal' => $codigopostal,'fechanacimiento' => $fechanacimiento,'email' => $email, 'password' => $password, 'tipo_usuario' => $tipo_usuario, 'fecha_alta' => $fecha_alta]);

     return redirect()->to('/listaempleados');
  }

  public function ver_usuario($id)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = UsuarioModel::
     select('id','usuario','apellidop','apellidom','telefono','calle','numero','colonia','municipio','estado','codigopostal','fechanacimiento','email','password','tipo_usuario'
     ,'fecha_alta')->where('id',$id)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\actualizarUsuario')
     ->with('usuarios',$ver_usuario1);
  }

  public function actualizar_usuario(Request $request)
  {
    $id = $request->id;
    $usuario = $request->usuario;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
     $calle = $request->calle;
     $numero = $request->numero;
     $colonia = $request->colonia;
     $municipio = $request->municipio;
     $estado = $request->estado;
     $codigopostal = $request->codigopostal;
     $fechanacimiento = $request->fechanacimiento;
    $email = $request->email;
    $password = $request->password;
    $tipo_usuario = $request->tipo_usuario;
    $fecha_alta = $request->fecha_registro;

    $ver_usuario1 = UsuarioModel::where('id',$id)
    ->update([
      'usuario' => $usuario,'apellidop' => $apellidop,'apellidom' => $apellidom,'telefono' => $telefono,'calle' => $calle,'numero' => $numero,'colonia' => $colonia,'municipio' => $municipio,'estado' => $estado,'codigopostal' => $codigopostal,'fechanacimiento' => $fechanacimiento,'email' => $email, 'password' => $password, 'tipo_usuario' => $tipo_usuario, 'fecha_alta' => $fecha_alta
    ]);

    return redirect()->to('/listaempleados');
  }

  public function ver_usuario_eli($id)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = UsuarioModel::
     select('id','usuario','apellidop','apellidom','telefono','calle','numero','colonia','municipio','estado','codigopostal','fechanacimiento','email','password','tipo_usuario'
     ,'fecha_alta')->where('id',$id)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\eliminarUsuario')
     ->with('usuarios',$ver_usuario1);
  }

  public function eliminar_usuario(Request $request)
  {
    $id = $request->id;
    $usuario = $request->usuario;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
     $calle = $request->calle;
     $numero = $request->numero;
     $colonia = $request->colonia;
     $municipio = $request->municipio;
     $estado = $request->estado;
     $codigopostal = $request->codigopostal;
     $fechanacimiento = $request->fechanacimiento;
    $email = $request->email;
    $password = $request->password;
    $tipo_usuario = $request->tipo_usuario;
    $fecha_alta = $request->fecha_registro;

    $ver_usuario1 = UsuarioModel::where('id',$id)
    ->delete();

    return redirect()->to('/listaempleados');
  }

  public function ver_usuario_bandera($id)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = UsuarioModel::
     select('id','usuario','apellidop','apellidom','telefono','calle','numero','colonia','municipio','estado','codigopostal','fechanacimiento','email','password','tipo_usuario'
     ,'fecha_alta')->where('id',$id)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Usuarios\ver_bandera')
     ->with('usuarios',$ver_usuario1);
  }

  public function eliminar_usuario_bandera(Request $request)
  {
    $id = $request->id;
    $ver_usuario1 = UsuarioModel::where('id',$id)
    ->update([
      'activo' => '0'
    ]);

    return redirect()->to('/listaempleados');
  }

  public function ver_tabla()
  {

     $tablaEmpleados = UsuarioModel::all();//->where('activo','1');
      return view('Users\ListarEmpleados')->with('todo', $tablaEmpleados);
  }
}
