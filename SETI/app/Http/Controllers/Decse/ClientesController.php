<?php
namespace App\Http\Controllers\Decse;
//se declara el controlador
use App\Http\Controllers\Controller;
use App\Categoria;
// declaracion del uso del controlador

// habilita request
use Illuminate\Http\Request;
use App\Models\Decse\ClienteModel;
class ClientesController extends Controller
{
	public function VistaAgregarCliente() {
      return view('Users/agregarClientes');
    }
    public function AgregarClientes(Request $request){

    $nombrec = $request->nombrec;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
    $email = $request->email;
    $empresa = $request->empresa;
    $calle = $request->calle;
    $numero = $request->numero;
    $colonia = $request->colonia;
    $municipio = $request->municipio;
    $estado = $request->estado;
    $codigopostal = $request->codigopostal;
    $referencia = $request->referencia;

//insercion
    ClienteModel::create(['nombrec' => $nombrec, 'apellidop' => $apellidop, 'apellidom' => $apellidom,'telefono' => $telefono,'email' => $email, 'empresa' => $empresa, 'calle' => $calle, 'numero' => $numero, 'colonia' => $colonia, 'municipio' => $municipio, 'estado' => $estado, 'codigopostal' => $codigopostal, 'referencia' => $referencia]);

return redirect()->to('/listaclientes');
    }


    public function ver_cliente($idcliente)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = ClienteModel::
     select('idcliente','nombrec','apellidop','apellidom','telefono','email','empresa','calle','numero','colonia','municipio','estado','codigopostal','referencia')->where('idcliente',$idcliente)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\actualizarClientes')
     ->with('cliente',$ver_usuario1);
  }

  public function actualizar_cliente(Request $request)
  {
    $idcliente = $request->idcliente;
    $nombrec = $request->nombrec;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
    $email = $request->email;
    $empresa = $request->empresa;
    $calle = $request->calle;
    $numero = $request->numero;
    $colonia = $request->colonia;
    $municipio = $request->municipio;
    $estado = $request->estado;
    $codigopostal = $request->codigopostal;
    $referencia = $request->referencia;

    $ver_usuario1 = ClienteModel::where('idcliente',$idcliente)
    ->update([
      'nombrec' => $nombrec, 'apellidop' => $apellidop, 'apellidom' => $apellidom,'telefono' => $telefono,'email' => $email, 'empresa' => $empresa, 'calle' => $calle, 'numero' => $numero, 'colonia' => $colonia, 'municipio' => $municipio, 'estado' => $estado, 'codigopostal' => $codigopostal, 'referencia' => $referencia
    ]);

    return redirect()->to('/listaempleados');
  }

  public function ver_cliente_eli($idcliente)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $ver_usuario1 = ClienteModel::
     select('idcliente','nombrec','apellidop','apellidom','telefono','email','empresa','calle','numero','colonia','municipio','estado','codigopostal','referencia')->where('idcliente',$idcliente)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\eliminarClientes')
     ->with('cliente',$ver_usuario1);
  }

  public function eliminar_cliente(Request $request)
  {
    $idcliente = $request->idcliente;
    $nombrec = $request->nombrec;
    $apellidop = $request->apellidop;
    $apellidom = $request->apellidom;
    $telefono = $request->telefono;
    $email = $request->email;
    $empresa = $request->empresa;
    $calle = $request->calle;
    $numero = $request->numero;
    $colonia = $request->colonia;
    $municipio = $request->municipio;
    $estado = $request->estado;
    $codigopostal = $request->codigopostal;
    $referencia = $request->referencia;

    $ver_usuario1 = ClienteModel::where('idcliente',$idcliente)
    ->delete();

    return redirect()->to('/listaempleados');
  }

    public function VerTablaClientes() {
      $tablaclientes = ClienteModel::all();//->where('activo','1');
      return view('Users/ListarClientes')->with('listadoC', $tablaclientes);
    }
    

}