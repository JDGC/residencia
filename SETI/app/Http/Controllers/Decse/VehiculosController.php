<?php
namespace App\Http\Controllers\Decse;
//se declara el controlador
use App\Http\Controllers\Controller;
use App\Categoria;
// declaracion del uso del controlador

// habilita request
use Illuminate\Http\Request;
/*
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Storage;
*/
// se declara modelos (bd)
use App\Models\Decse\UnidadTrasnporteModel;
class VehiculosController extends Controller
{
	public function VistaAgregarVehiculos() {
      return view('Users/AgregarVehiculos');
    }
    public function AgregarVehiculos(Request $request){

    $tipotransporte = $request->tipotransporte;
    $modelo = $request->modelo;
    $estado = $request->estado;
    $fechaservicio = $request->fechaservicio;
//insercion
    UnidadTrasnporteModel::create(['tipotransporte' => $tipotransporte, 'modelo' => $modelo, 'estado' => $estado,'fechaservicio' => $fechaservicio]);

return redirect()->to('/listaV');
    }


    public function ver_Vehiculos($idunidad)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $editar_vehiculo = UnidadTrasnporteModel::
     select('idunidad','tipotransporte','modelo','estado','fechaservicio')->where('idunidad',$idunidad)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\actualizarVehiculo')
     ->with('vehiculo',$editar_vehiculo);
  }

  public function actualizar_Vehiculos(Request $request)
  {
    $idunidad = $request->idunidad;
    $tipotransporte = $request->tipotransporte;
    $modelo = $request->modelo;
    $estado = $request->estado;
    $fechaservicio = $request->fechaservicio;

    $ver_usuario1 = UnidadTrasnporteModel::where('idunidad',$idunidad)
    ->update([
      'tipotransporte' => $tipotransporte, 'modelo' => $modelo, 'estado' => $estado,'fechaservicio' => $fechaservicio]);

     return redirect()->to('/listaV');
  }

  public function ver_Vehiculos_eli($idunidad)
  {
     //$ver_usuario = UsuarioModel::all();
     // select * from usuarios;
     $eliminar_vehiculo = UnidadTrasnporteModel::
     select('idunidad','tipotransporte','modelo','estado','fechaservicio')->where('idunidad',$idunidad)->first();

     //return $ver_usuario1;
     // select usuario,password,tipo_usuario
    // where id = 1;
     return view('Users\eliminarVehiculo')
     ->with('vehiculo',$eliminar_vehiculo);
  }

  public function eliminar_Vehiculos(Request $request)
  {
    $idunidad = $request->idunidad;
    $tipotransporte = $request->tipotransporte;
    $modelo = $request->modelo;
    $estado = $request->estado;
    $fechaservicio = $request->fechaservicio;

    $eliminar_vehiculo = UnidadTrasnporteModel::where('idunidad',$idunidad)
    ->delete();

    return redirect()->to('/listaV');
  }

 public function VerTablaVehiculos() {
      $tablaVehiculos = UnidadTrasnporteModel::all();//->where('activo','1');
      return view('Users/ListarVehiculos')->with('listadoV', $tablaVehiculos);
  }
}
