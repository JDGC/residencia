<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>{{ config('app.name', 'Decse Technology') }}</title>

    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->

    
    <!-- MDB icon -->
    <link rel="icon" href="{{url('../images/logo.ico')}}" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">

    <!-- Bootstrap core CSS -->
    {!! Html::style('../css/bootstrap.min.css') !!}
    <!-- Material Design Bootstrap -->
    {!! Html::style('../css/mdb.min.css') !!}

    <!-- Your custom styles (optional) -->
    {!! Html::style('../css/style.css') !!}

    {!! Html::style('../css/residencia.css') !!}

    <script type="text/javascript" src="js/jquery.min.js" ></script> <!-- esta en public -->
    <script type="text/javascript" src="js/servicio.js" ></script>

</head>
<body onload="funcion_carga();">
        <hr>
            
            @yield('content')
    </div>
</body>
<!-- jQuery -->
    {!! Html::script('assets/MD-Boostrap/js/jquery.min.js') !!}

  <!-- Bootstrap tooltips -->
  {!! Html::script('assets/MD-Boostrap/js/popper.min.js') !!}

  <!-- Bootstrap core JavaScript -->
  {!! Html::script('assets/MD-Boostrap/js/bootstrap.min.js') !!}

  <!-- MDB core JavaScript -->
  {!! Html::script('assets/MD-Boostrap/js/mdb.min.js') !!}

  <!-- CARGAMOS NUESTRAS FUNCIONES DE JavaScript -->
  {!! Html::script('assets/js/misFunciones.js') !!}
</html>
