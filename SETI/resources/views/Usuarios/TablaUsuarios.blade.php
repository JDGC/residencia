<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <a href="{{ url('/Usuario/Altas') }}">Alta</a>
    <br><br>

    <table border="1">
             <tr>
                    <td>ID</td>
                    <td>Nombre de usuario</td>
                    <td>Apellido Paterno</td>
                    <td>Apellido Materno</td>
                    <td>telefono</td>
                    <td>calle</td>
                    <td>numero</td>
                    <td>colonia</td>
                    <td>municipio</td>
                    <td>estado</td>
                    <td>codigo postal</td>
                    <td>fecha nacimiento</td>
                    <td>email</td>
                    <td>Contrase&ntilde;a</td>
                    <td>Tipo de usuario</td>
                    <td>Fecha alta</td>
                    <td>Estatus</td>
                    <td>Opciones</td>
             </tr>

             @foreach($todo as $x)
             <tr>
                    <td>{{ $x->id }}</td>
                    <td>{{ $x->usuario }}</td>
                    <td>{{ $x->apellidop }}</td>
                    <td>{{ $x->apellidom }}</td>
                    <td>{{ $x->telefono }}</td>
                    <td>{{ $x->calle }}</td>
                    <td>{{ $x->numero }}</td>
                    <td>{{ $x->colonia }}</td>
                    <td>{{ $x->municipio }}</td>
                    <td>{{ $x->estado }}</td>
                    <td>{{ $x->codigopostal }}</td>
                    <td>{{ $x->fechanacimiento }}</td>
                    <td>{{ $x->email }}</td>
                    <td>{{ $x->password }}</td>
                    <td>{{ $x->tipo_usuario }}</td>
                    <td>{{ $x->fecha_alta }}</td>
                    <td>
                      @if($x->activo == '0')
                             {{ 'Inactivo' }}
                      @else
                             {{ 'Activo' }}
                      @endif
                    </td>
                    <td>
                      <a href="/Usuario/Editar/{{ $x->id }}">Editar</a>
                      | <a href="/Usuario/Eliminar/{{ $x->id }}">Eliminar</a>
                      | <a href="/Usuario/Eliminar_bandera/{{ $x->id }}">Baja</a>
                    </td>
             </tr>
             @endforeach
    </table>
  </body>
</html>
