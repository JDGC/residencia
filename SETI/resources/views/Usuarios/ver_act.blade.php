<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <form action="{{ url('/Usuario/Actualizar') }}" method="post">

    @csrf

    <input type="hidden" name="id" value="{{ $usuarios->id }}">

           <label for="">Usuario</label>

           <input type="text" value="{{ $usuarios->usuario }}" name="usuario">

           <label for="">Apellido Paterno</label>

           <input type="text" value="{{ $usuarios->apellidop }}" name="apellidop">

           <label for="">Apellido Materno</label>

           <input type="text" value="{{ $usuarios->apellidom }}" name="apellidom">

           <label for="">telefono</label>

           <input type="text" value="{{ $usuarios->telefono }}"name="telefono">
           <label for="">calle</label>

           <input type="text" value="{{ $usuarios->calle }}" name="calle">
           <label for="">numero</label>

           <input type="text" value="{{ $usuarios->numero }}" name="numero">
           <label for="">colonia</label>

           <input type="text" value="{{ $usuarios->colonia }}" name="colonia">
           <label for="">municipio</label>

           <input type="text" value="{{ $usuarios->municipio }}" name="municipio">
           <label for="">estado</label>

           <input type="text" value="{{ $usuarios->estado }}" name="estado">
           <label for="">codigopostal</label>

           <input type="text" value="{{ $usuarios->codigpostal }}" name="codigopostal">
           <label for="">fechanacimiento</label>

           <input type="text" value="{{ $usuarios->fechanacimiento }}" name="fechanacimiento">

           <label>Password</label>
           <input type="password" value="{{ $usuarios->password }}" name="pass">

          <label for="">Tipo de usuario</label>
           <select class="" name="tipo_usuario">
             <option value="">Escoja opcion</option>
             <option value="0">Administrador</option>
             <option value="1">Temporal</option>
           </select>

           <label for="">Fecha de ingreso</label>
           <input type="date" value="{{ $usuarios->fecha_alta }}" name="fecha_registro"></input>

           <input type="submit" name="act" value="Actualizar">
      </form>
    </body>
</html>
