<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DECSE TECHNOLOGY</title>
    <!--<link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="../css/residencia.css">-->
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../css/login.css" rel="stylesheet" type="text/css" />
    <link href="../css/components.css" rel="stylesheet" id="style_components" type="text/css" />
    <!--<link rel="stylesheet" href="css/style.css">-->
  </head>
  <body class="login">
    <div class="container">
      <hr>
      @yield('content')
    </div>
  </body>
</html>
<!--https://www.youtube.com/watch?v=_vfIXJsqrIo
  https://aprendible.com/series/laravel-desde-cero/lecciones/rutas-con-nombre-en-laravel
  -->
