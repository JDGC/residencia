@extends('layouts.app')

@section('content')
<!-- --> 

   <!--/.Navbar danger color-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark danger-color scrolling-navbar">
        <div class="container">

                <a class="navbar-brand" href="{{ url('/dashboard') }}">
                    <img src="{{ url('../images/logo.jpg') }}" height="40" class="align-top" alt="mdb logo"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/dashboard') }}">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-edit"></i>
                        Consultar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/listaempleados') }}">EMPLEADO</a>
                        <a class="dropdown-item" href="{{ url('/listaclientes') }}">CLIENTES</a>
                        <a class="dropdown-item" href="{{ url('/listaSolicitud') }}">SERVICIOS</a>
                        <a class="dropdown-item" href="{{ url('/listaV') }}">VEHICULOS</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-redo"></i>
                        Nuevo
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/Servicios/Altas') }}">Solicitar Servicio</a>
                        <a class="dropdown-item" href="{{ url('/Cliente/Altas') }}">Buscar Cliente</a>
                        <a class="dropdown-item" href="{{ url('/Usuario/Altas') }}">Empleados</a>
                        <a class="dropdown-item" href="{{ url('/Vehiculo/Altas') }}">Vehiculos</a>
                        
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i>
                        Generar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Reportes Tecnicos</a>
                        <a class="dropdown-item" href="#">Reportes por fecha</a>
                        </div>
                    </li>

                </ul>
            </div>

            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Bienvenido 
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">{{ Auth::user()->usuario }} <span class="caret"></span></a>
                                    <a class="dropdown-item" href="{{ url('/Registro_Actividades') }}">Perfil</a>
                                    <a class="dropdown-item" href="{{ url('/cuenta') }}">Cuenta</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

            </ul>

        </div>

    </nav>
    <br>
    <br>

<!-- COMENZAMOS CON EL CODIGO DEL FORMULARIO  -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                 <div class="card-header" style="font-size:30px;">Registro del Empleado :: Decse Technology</div>

                         <form action="{{ url('/Usuario/Actualizar') }}" method="post">
                            @csrf

                            <input type="hidden" name="id" value="{{ $usuarios->id }}">

                            <h1 style="font-size:25px;">Datos generales</h1>
                            <div style="border:2px solid #17a2b8; padding: 5px; border-radius: 0.5em;">

                            <!-- NOMBRE DEL CLIENTE "Nombre" -->
                            <div class="form-group row">
                                <label for="usuario" class="col-md-4 col-form-label text-md-right">Nombre: </label>
                                <div class="col-md-6">
                                    <input id="usuario"
                                        style="width:250px"
                                        type="text"
                                        class="form-control"
                                        value="{{ $usuarios->usuario }}"
                                        name="usuario"
                                        maxlength="20"
                                        minlength="3"
                                        placeholder="'Juan Diego'">
                                </div>
                            </div>

                            <!--APELLIDO PATERNO DEL CLIENTE "Apellido_Paterno" -->
                            <div class="form-group row">
                                <label for="Apellido_Paterno" class="col-md-4 col-form-label text-md-right">Apellido_Paterno: </label>
                                <div class="col-md-6">
                                    <input id="apellidop"
                                        type="text"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->apellidop }}"
                                        name="apellidop"
                                        placeholder="'Diaz'"
                                        maxlength="20"
                                        minlength="3">
                                </div>
                            </div>

                            <!-- APELLIDO MATERNO DEL CLIENTE "Apellido_Materno" -->
                            <div class="form-group row">
                                <label for="Apellido_Materno" class="col-md-4 col-form-label text-md-right">Apellido_Materno: </label>

                                <div class="col-md-6">
                                    <input id="apellidom"
                                        type="text"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->apellidom }}"
                                        name="apellidom"
                                        placeholder="'Alcantara'"
                                        maxlength="20"
                                        minlength="3">
                                </div>
                            </div>

                            <!-- TELEFONO (tipousuario) -->
                            <div class="form-group row">
                                <label for="tipo_usuario" class="col-md-4 col-form-label text-md-right">Tipo de Usuario: </label>
                                <div class="col-md-6">
                                    <select class="custom-select" id="tipo_usuario" value="{{ $usuarios->tipo_usuario }}"name="tipo_usuario" style="width:250px" value="" required autofocus>
                                        <option value="0" selected>Elegir: </option>
                                            <option value="1">Administrador</option>
                                            <option value="2">Encargado de Area</option>
                                            <option value="3">Directivo de Ventas</option>
                                            <option value="4">Tecnico</option>
                                            <option value="5">Mantenimiento</option>
                                    </select>
                                </div>
                            </div>

                            <!--   -->
                            <div class="form-group row">
                                <label for="Telefono" class="col-md-4 col-form-label text-md-right">Telefono:</label>
                                <div class="col-md-6">
                                    <input id="telefono"
                                        type="bigint"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->telefono }}"
                                        name="telefono"
                                        placeholder="'9518568263'">
                                </div>
                            </div>
                            <!----- correo de contacto de empleado-->
                            <div class="form-group row">
                                <label for="Email" class="col-md-4 col-form-label text-md-right">Correo Electronico: </label>

                                <div class="col-md-6">
                                    <input id="email"
                                        type="email"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->email }}"
                                        name="email"
                                        placeholder="'example@example.com'"
                                        maxlength="20"
                                        minlength="3">
                                </div>
                            </div>

                            <!----- contraseña de empleado-->
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña: </label>

                                <div class="col-md-6">
                                    <input id="password"
                                        type="text"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->password }}"
                                        name="password"
                                        placeholder="'contraseña'"
                                        maxlength="20"
                                        minlength="3">
                                </div>
                            </div>
                        </div>

                        <h1 style="font-size:25px;">Direccion</h1>
                            <div style="border:2px solid #17a2b8; padding: 5px; border-radius: 0.5em;">

                            <!-- DOMICILIO DEL CHOFER  "domicilio"-->
                            <div class="form-group row">
                                <label for="Calle" class="col-md-4 col-form-label text-md-right">Calle:</label>
                                <div class="col-md-6">
                                    <input  id="calle"
                                            type="text"
                                            class="form-control"
                                            value="{{ $usuarios->calle }}"
                                            name="calle">
                                </div>
                            </div>

                            <!-- Numero"-->
                            <div class="form-group row">
                                <label for="Numero" class="col-md-4 col-form-label text-md-right">Numero:</label>
                                <div class="col-md-6">
                                    <input  id="numero"
                                            type="text"
                                            class="form-control"
                                            value="{{ $usuarios->numero }}"
                                            name="numero">
                                </div>
                            </div>

                            <!-- COLONIA -->
                            <div class="form-group row">
                                <label for="Colonia" class="col-md-4 col-form-label text-md-right">Colonia: </label>
                                    <div class="col-md-6">
                                        <input  id="colonia"
                                                type="text"
                                                class="form-control"
                                                value="{{ $usuarios->colonia }}"
                                                name="colonia">
                                    </div>
                            </div>

                            <!-- MUNICIPIO  -->
                            <div class="form-group row">
                                <label for="Municipio" class="col-md-4 col-form-label text-md-right">Municipio: </label>
                                <div class="col-md-6">
                                    <input id="municipio"
                                        type="text"
                                        class="form-control"
                                        value="{{ $usuarios->municipio }}"
                                        name="municipio">
                                </div>
                            </div>

                            <!-- ESTADO -->
                            <div class="form-group row">
                                <label for="Estado" class="col-md-4 col-form-label text-md-right">Estado: </label>
                                    <div class="col-md-6">
                                        <input  id="estado"
                                                type="text"
                                                class="form-control"
                                                value="{{ $usuarios->estado }}"
                                                name="estado">
                                    </div>
                            </div>

                            <!-- MUNICIPIO  -->
                            <div class="form-group row">
                                <label for="Codigo_Postal" class="col-md-4 col-form-label text-md-right">Codigo Postal: </label>
                                <div class="col-md-6">
                                    <input id="codigopostal"
                                        type="text"
                                        class="form-control"
                                        value="{{ $usuarios->codigpostal }}"
                                        name="codigopostal">
                                </div>
                            </div>

                            <!-- FECHA DE NACIMINETO "Fecha" -->
                            <div class="form-group row">
                                <label for="fechanacimiento" class="col-md-4 col-form-label text-md-right">Fecha de Nacimiento: </label>
                                <div class="col-md-6">
                                    <input id="fechanacimiento"
                                        type="date"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->fechanacimiento }}"
                                        name="fechanacimiento">
                                </div>
                            </div>

                            <!-- FECHA DE NACIMINETO "Fecha" -->
                            <div class="form-group row">
                                <label for="fecha_registro" class="col-md-4 col-form-label text-md-right">fecha de Ingreso: </label>
                                <div class="col-md-6">
                                    <input id="fecha_registro"
                                        type="date"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $usuarios->fecha_alta }}"
                                        name="fecha_registro">
                                </div>
                            </div>

                        <br>

                        <!-- ENVIAR FORMULARIO-->
                        <div class="form-group row justify-content-center">
                            <input class="btn btn-danger" type="submit" value="Registrar Empelado" id="btn_registrar">
                        </div>
                        
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

    <br>
    <br>
    <br>

    <!-- Footer -->
    <footer>
      <div class="footer-copy-redes" style="background-color:#2E86C1;">
        <div class="main-copy-redes">
          <div class="footer-copy">
            <center>   <div class="page-footer-inner"> <?php echo  $hoy2 = date("Y"); ?>
                <a  title="Contacta con +58 0426-8734726" target="_blank">SITIO OFICIAL DECSE TECHNOLOGY</a></center> 
          </div>
          <div class="footer-redes">
            <li><a href="https://www.twitter.com" class="" title="twitter">
            <img class="crafty-social-button-image"alt="twitter" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/twitter.png">
            </a>
            </li>
            <li><a href="https://www.facebook.com/decsetechnology" class="" title="facebook">
            <img class="crafty-social-button-image"alt="facebook" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/facebook.png">
            </a>
            </li>
            <li><a href="https://wwww.gmail.com" class="l" title="Email">
            <img class="crafty-social-button-image"alt="Email" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/email.png">
            </a>
            </li>
          </div>
        </div>
      </div>
</div>
    </footer> 
    <!-- Footer -->
@endsection