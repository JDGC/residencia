@extends('layouts.app')

@section('content')
<!-- -->

    <!--/.Navbar danger color-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark danger-color scrolling-navbar">
        <div class="container">

                <a class="navbar-brand" href="{{ url('/dashboard') }}">
                    <img src="{{ url('../images/logo.jpg') }}" height="40" class="align-top" alt="mdb logo"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/dashboard') }}">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-edit"></i>
                        Consultar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/listaempleados') }}">EMPLEADO</a>
                        <a class="dropdown-item" href="{{ url('/listaclientes') }}">CLIENTES</a>
                        <a class="dropdown-item" href="{{ url('/listaSolicitud') }}">SERVICIOS</a>
                        <a class="dropdown-item" href="{{ url('/listaV') }}">VEHICULOS</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-redo"></i>
                        Nuevo
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/Servicios/Altas') }}">Solicitar Servicio</a>
                        <a class="dropdown-item" href="{{ url('/Cliente/Altas') }}">Buscar Cliente</a>
                        <a class="dropdown-item" href="{{ url('/Usuario/Altas') }}">Empleados</a>
                        <a class="dropdown-item" href="{{ url('/Vehiculo/Altas') }}">Vehiculos</a>
                        
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i>
                        Generar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Reportes Tecnicos</a>
                        <a class="dropdown-item" href="#">Reportes por fecha</a>
                        </div>
                    </li>

                </ul>
            </div>

            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Bienvenido 
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">{{ Auth::user()->usuario }} <span class="caret"></span></a>
                                    <a class="dropdown-item" href="{{ url('/Registro_Actividades') }}">Perfil</a>
                                    <a class="dropdown-item" href="{{ url('/cuenta') }}">Cuenta</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

            </ul>

        </div>

    </nav>
    <br>
    <br>

<!-- COMENZAMOS CON EL CODIGO DEL FORMULARIO  -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                 <div class="card-header" style="font-size:30px;">Lista de Empleados :: Decse Technology</div>

                    <div class="card-body">
                   <!--<a style=" margin-left: 10px;" title="Registrar Nuevo" class="btn red btn-outline sbold " href="/Insertar">Nuevo </a>-->
                          <a style=" margin-left: 10px;" class="btn red btn-outline sbold " title="Actualizar tabla" data-toggle="modal" href="/listaS">actualizar<i class="fa fa-refresh" aria-hidden="true"></i> </a>
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>

                                    </div>

                                    <div class="tools "> </div>


                                </div>


                                <div class="portlet-body">

                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">

      <thead>
        <tr>
                    <th class="min-phone-l">#</th>
                  <th class="min-phone-l">nombre completo </th>
                  <th class="none">Contacto</th>
                  <th class="none">Tipo Empleado</th>
                  <th class="min-phone-l">direccion</th>                  
                  <th class="min-phone-l">estatus</th>
                  <th class="min-phone-l"> Opciones</th>
             </tr>

             @foreach($todo as $x)
             <tr>
                    <td width="1%">{{ $x->id }}</td>
                    <td width="3%">{{ $x->usuario }}<br>{{ $x->apellidop }} {{ $x->apellidom }}</td>
                    <td width="3%">{{ $x->telefono }}<br>{{ $x->email }}</td>
                    <td width="2%">{{ $x->tipo_usuario }}</td>
                    <td width="10%">{{ $x->calle }} {{ $x->numero }}<br>{{ $x->colonia }}{{ $x->municipio }}<br>{{ $x->estado }}{{ $x->codigopostal }}</td>
                    <td width="3%">
                      @if($x->activo == '0')
                             {{ 'Inactivo' }}
                      @else
                             {{ 'Activo' }}
                      @endif
                    </td>
                    <td width="15%">
        <center>
                <a class="dt-button buttons-pdf buttons-html5 btn blue btn-outline "  href="/Usuario/Editar/{{ $x->id }}" title="editar" id="buttonHola"><i class='fa fa-edit'></i> </a>
                <a class="dt-button buttons-pdf buttons-html5 btn blue btn-outline  " data-toggle="modal"  title="Ver" href="#productover" id="buttonHola" onclick="myFunction(this, '')" ><i class='fa fa-eye'></i></a>
                <a  class="btn red btn-outline sbold derecha"  title="Eliminar" class="btn red btn-outline sbold"  href="/Usuario/Eliminar/{{ $x->id }}"><i class=' fa fa-trash'></i></a>
                <a href="/Usuario/Eliminar_bandera/{{ $x->id }}">Baja</a>
        </center>
        </td>
             </tr>
             @endforeach
    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>

                    </div>
                  </div>
              </div>
        </div>
        </div>
        
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>

    <!-- Footer -->
    <footer>
      <div class="footer-copy-redes" style="background-color:#2E86C1;">
        <div class="main-copy-redes">
          <div class="footer-copy">
            <center>   <div class="page-footer-inner"> <?php echo  $hoy2 = date("Y"); ?>
                <a  title="Contacta con +58 0426-8734726" target="_blank">SITIO OFICIAL DECSE TECHNOLOGY</a></center> 
          </div>
          <div class="footer-redes">
            <li><a href="https://www.twitter.com" class="" title="twitter">
            <img class="crafty-social-button-image"alt="twitter" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/twitter.png">
            </a>
            </li>
            <li><a href="https://www.facebook.com/decsetechnology" class="" title="facebook">
            <img class="crafty-social-button-image"alt="facebook" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/facebook.png">
            </a>
            </li>
            <li><a href="https://wwww.gmail.com" class="l" title="Email">
            <img class="crafty-social-button-image"alt="Email" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/email.png">
            </a>
            </li>
          </div>
        </div>
      </div>
</div>
    </footer> 
    <!-- Footer -->
@endsection