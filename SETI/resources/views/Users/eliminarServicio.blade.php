@extends('layouts.app')

@section('content')
<!-- --> 
<!--/.Navbar danger color-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark danger-color scrolling-navbar">
        <div class="container">

                <a class="navbar-brand" href="{{ url('/dashboard') }}">
                    <img src="{{ url('../images/logo.jpg') }}" height="40" class="align-top" alt="mdb logo"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/dashboard') }}">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-edit"></i>
                        Consultar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/listaempleados') }}">EMPLEADO</a>
                        <a class="dropdown-item" href="{{ url('/listaclientes') }}">CLIENTES</a>
                        <a class="dropdown-item" href="{{ url('/listaSolicitud') }}">SERVICIOS</a>
                        <a class="dropdown-item" href="{{ url('/listaV') }}">VEHICULOS</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-redo"></i>
                        Nuevo
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/Servicios/Altas') }}">Solicitar Servicio</a>
                        <a class="dropdown-item" href="{{ url('/Cliente/Altas') }}">Buscar Cliente</a>
                        <a class="dropdown-item" href="{{ url('/Usuario/Altas') }}">Empleados</a>
                        <a class="dropdown-item" href="{{ url('/Vehiculo/Altas') }}">Vehiculos</a>
                        
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i>
                        Generar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Reportes Tecnicos</a>
                        <a class="dropdown-item" href="#">Reportes por fecha</a>
                        </div>
                    </li>

                </ul>
            </div>

            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Bienvenido 
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">{{ Auth::user()->usuario }} <span class="caret"></span></a>
                                    <a class="dropdown-item" href="{{ url('/Registro_Actividades') }}">Perfil</a>
                                    <a class="dropdown-item" href="{{ url('/cuenta') }}">Cuenta</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

            </ul>

        </div>

    </nav>

    <br>
    <br>

<!-- COMENZAMOS CON EL CODIGO DEL FORMULARIO  -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                 <div class="card-header" style="font-size:30px;">Eliminar Cliente :: Decse Technology</div>

                         <form action="{{ url('/Servicios/Baja') }}" method="post">
                            @csrf

                            <input type="hidden" name="idservicio" value="{{ $servicio->idservicio }}">

                            <h1 style="font-size:25px;">Datos generales</h1>
                            <div style="border:2px solid #17a2b8; padding: 5px; border-radius: 0.5em;">

                            <!-- NOMBRE DEL CLIENTE "Nombre" -->
                            <div class="form-group row">
                                <label for="tiposervicio" class="col-md-4 col-form-label text-md-right">Tipo del servicio: </label>
                                <div class="col-md-6">
                                    <input id="tiposervicio"
                                        style="width:250px"
                                        type="text"
                                        class="form-control"
                                        value="{{ $servicio->tiposervicio }}"
                                        name="tiposervicio"
                                        maxlength="20"
                                        minlength="3"
                                        placeholder="'Juan Diego'">
                                </div>
                            </div>

                            <!--APELLIDO PATERNO DEL CLIENTE "Apellido_Paterno" -->
                            <div class="form-group row">
                                <label for="fecha" class="col-md-4 col-form-label text-md-right">fecha: </label>
                                <div class="col-md-6">
                                    <input id="fecha"
                                        type="date"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $servicio->fecha }}"
                                        name="fecha"
                                        placeholder="'Diaz'"
                                        maxlength="20"
                                        minlength="3">
                                </div>
                            </div>

                            <!-- APELLIDO MATERNO DEL CLIENTE "Apellido_Materno" -->
                            <div class="form-group row">
                                <label for="Hora" class="col-md-4 col-form-label text-md-right">Hora: </label>

                                <div class="col-md-6">
                                    <input id="hora"
                                        type="time"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $servicio->hora }}"
                                        name="hora"
                                        placeholder="'Alcantara'"
                                        maxlength="20"
                                        minlength="3">
                                </div>
                            </div>

                            <!--   -->
                            <div class="form-group row">
                                <label for="descripcion" class="col-md-4 col-form-label text-md-right">Descripcion:</label>
                                <div class="col-md-6">
                                    <input id="descripcion"
                                        type="text"
                                        style="width:250px"
                                        class="form-control"
                                        value="{{ $servicio->descripcion }}"
                                        name="descripcion">
                                </div>
                            </div>
                        <br>

                        <!-- ENVIAR FORMULARIO-->
                        <div class="form-group row justify-content-center">
                            <input class="btn btn-danger" type="submit" value="Eliminar Cliente" id="btn_registrar">
                        </div>
                        
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <br>
    <br>
    <br>

    <!-- Footer -->
    <footer>
      <div class="footer-copy-redes" style="background-color:#2E86C1;">
        <div class="main-copy-redes">
          <div class="footer-copy">
            <center>   <div class="page-footer-inner"> <?php echo  $hoy2 = date("Y"); ?>
                <a  title="Contacta con +58 0426-8734726" target="_blank">SITIO OFICIAL DECSE TECHNOLOGY</a></center> 
          </div>
          <div class="footer-redes">
            <li><a href="https://www.twitter.com" class="" title="twitter">
            <img class="crafty-social-button-image"alt="twitter" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/twitter.png">
            </a>
            </li>
            <li><a href="https://www.facebook.com/decsetechnology" class="" title="facebook">
            <img class="crafty-social-button-image"alt="facebook" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/facebook.png">
            </a>
            </li>
            <li><a href="https://wwww.gmail.com" class="l" title="Email">
            <img class="crafty-social-button-image"alt="Email" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/email.png">
            </a>
            </li>
          </div>
        </div>
      </div>
</div>
    </footer> 
    <!-- Footer -->
@endsection