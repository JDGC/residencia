@extends('layouts.app')

@section('content')
<!-- -->

    <!--/.Navbar danger color-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark danger-color scrolling-navbar">
        <div class="container">

                <a class="navbar-brand" href="{{ url('/dashboard') }}">
                    <img src="{{ url('../images/logo.jpg') }}" height="40" class="align-top" alt="mdb logo"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/dashboard') }}">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-edit"></i>
                        Consultar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/listaempleados') }}">EMPLEADO</a>
                        <a class="dropdown-item" href="{{ url('/listaclientes') }}">CLIENTES</a>
                        <a class="dropdown-item" href="{{ url('/listaSolicitud') }}">SERVICIOS</a>
                        <a class="dropdown-item" href="{{ url('/listaV') }}">VEHICULOS</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-redo"></i>
                        Nuevo
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/Servicios/Altas') }}">Solicitar Servicio</a>
                        <a class="dropdown-item" href="{{ url('/Cliente/Altas') }}">Buscar Cliente</a>
                        <a class="dropdown-item" href="{{ url('/Usuario/Altas') }}">Empleados</a>
                        <a class="dropdown-item" href="{{ url('/Vehiculo/Altas') }}">Vehiculos</a>
                        
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i>
                        Generar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Reportes Tecnicos</a>
                        <a class="dropdown-item" href="#">Reportes por fecha</a>
                        </div>
                    </li>

                </ul>
            </div>

            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Bienvenido 
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">{{ Auth::user()->usuario }} <span class="caret"></span></a>
                                    <a class="dropdown-item" href="{{ url('/Registro_Actividades') }}">Perfil</a>
                                    <a class="dropdown-item" href="{{ url('/cuenta') }}">Cuenta</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

            </ul>

        </div>

    </nav>

    <br>
    <br>

                <div class="container">
            <div class="col-md-12">
                <div class="card">

                                 <div class="card-body">
                                        <i class="icon-share font-blue"></i>
                                        <center><h5 class=”panel-title”> Notificaciones Importantes del Dia : {{ auth()->user()->name }}</h5></center>
                                    </div>
                                    <div class="card-body" style="align-content: center;">
                                   <div id="chartContainer2" style="height: 400; width: 1070px;">
                                    <!--<img src="images/decse.jpg" style="height: 500px; width: 100%;">-->
                                     <!--<div id="carouselControls" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">-->
                                   <div class="slider">
                <ul>
                    <li>
      <img src="images/decse.jpg">
     </li>
                    <li>
      <img src="images/fondo.png">
    </li>
                </ul>
            </div>
        </div>
    </div>
                        </div>

                                </div>
                                
                        
        <center><div class="saludo">
            <p>SISTEMA WEB DESARROLLADO PARA EL CONTROL DE LOS SERVICIOS DE LA EMPRESA DECSE TECHNOLOGY</p>
        </div></center>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>

    </div>

    <footer>
      <div class="footer-copy-redes" style="background-color:#2E86C1;">
        <div class="main-copy-redes">
          <div class="footer-copy">
            <center>   <div class="page-footer-inner"> <?php echo  $hoy2 = date("Y"); ?>
                <a  title="Contacta con +58 0426-8734726" target="_blank">SITIO OFICIAL DECSE TECHNOLOGY</a></center> 
          </div>
          <div class="footer-redes">
            <li><a href="https://www.twitter.com" class="" title="twitter">
            <img class="crafty-social-button-image"alt="twitter" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/twitter.png">
            </a>
            </li>
            <li><a href="https://www.facebook.com/decsetechnology" class="" title="facebook">
            <img class="crafty-social-button-image"alt="facebook" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/facebook.png">
            </a>
            </li>
            <li><a href="https://wwww.gmail.com" class="l" title="Email">
            <img class="crafty-social-button-image"alt="Email" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/email.png">
            </a>
            </li>
          </div>
        </div>
      </div>
</div>
    </footer>      
@endsection
