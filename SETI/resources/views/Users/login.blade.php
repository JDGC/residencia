@extends('Users.principal')

@section('content')
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      </a>
      <div class="panel panel-default">

        <div class="panel-heading">
                 <center><img width="90%" src='../images/logo.png'></center>
        </div>
        <div class="panel-body">
            <form method="post" action="{{ route('login') }}">
              {{ csrf_field() }}
              <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <center><label for="email" id="email">Email</label></center>
                <input class="form-control"
                type="email"
                name="email"
                values = "{{ old('email') }}"
                placeholder="ingresa tu email">
                {!! $errors->first('email','<span class="help-block">:message</span>') !!}
              </div>
              <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <center><label for="password" id="password">Contraseña</label></center>
                <input class="form-control"
                type="password"
                name="password"
                placeholder="ingresa tu password">
                {!! $errors->first('password','<span class="help-block">:message</span>') !!}
              </div>
              <div class="container-login100-form-btn">
              <button class="btn btn-primary btn-block"> Inicia sesión</button>
            </div class="text-center p-t-57 p-b-20">
            <br>
            <div class="text-center p-t-57 p-b-20">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Olvido su contraseña?') }}
                                    </a>
                                @endif
            </div>
      </form>
    </div>
</div>
@endsection