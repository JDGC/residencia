@extends('layouts.app')

@section('content')
<!-- -->
    <!--/.Navbar danger color-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark danger-color scrolling-navbar">
        <div class="container">

                <a class="navbar-brand" href="{{ url('/dashboard') }}">
                    <img src="{{ url('../images/logo.jpg') }}" height="40" class="align-top" alt="mdb logo"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/dashboard') }}">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-edit"></i>
                        Consultar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/listaempleados') }}">EMPLEADO</a>
                        <a class="dropdown-item" href="{{ url('/listaclientes') }}">CLIENTES</a>
                        <a class="dropdown-item" href="{{ url('/listaSolicitud') }}">SERVICIOS</a>
                        <a class="dropdown-item" href="{{ url('/listaV') }}">VEHICULOS</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-redo"></i>
                        Nuevo
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ url('/Servicios/Altas') }}">Solicitar Servicio</a>
                        <a class="dropdown-item" href="{{ url('/Cliente/Altas') }}">Buscar Cliente</a>
                        <a class="dropdown-item" href="{{ url('/Usuario/Altas') }}">Empleados</a>
                        <a class="dropdown-item" href="{{ url('/Vehiculo/Altas') }}">Vehiculos</a>
                        
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i>
                        Generar
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Reportes Tecnicos</a>
                        <a class="dropdown-item" href="#">Reportes por fecha</a>
                        </div>
                    </li>

                </ul>
            </div>

            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Bienvenido 
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">{{ Auth::user()->usuario }} <span class="caret"></span></a>
                                    <a class="dropdown-item" href="{{ url('/Registro_Actividades') }}">Perfil</a>
                                    <a class="dropdown-item" href="{{ url('/cuenta') }}">Cuenta</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

            </ul>

        </div>

    </nav>

    <br>
    <br>

              <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                 <div class="card-header" style="font-size:30px;">Listar Servicios :: Decse Technology</div>

                    <div class="card-body">
                          <a style=" margin-left: 10px;" title="Registrar Nuevo" class="btn red btn-outline sbold " href="/servicionuevo">Nuevo </a>
                          <a style=" margin-left: 10px;" class="btn red btn-outline sbold " title="Actualizar tabla" data-toggle="modal" href="/listaSS"> <i class="fa fa-refresh" aria-hidden="true"></i> </a>
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>

                                    </div>

                                    <div class="tools "> </div>

                                </div>

                                <div class="portlet-body">

                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">

      <thead>
        <tr>
                  <th  class="all hidden-print">#</th>
                                                <th class="min-phone-l">cliente</th>
                                                <th class="min-phone-l">servicio</th>
                                                <th class="none">No. Servicio</th>
                                                <th class="min-phone-l"> Opciones</th>

        </tr>
       </thead>

      @foreach ($soliciud as $soli)

      <tr>
        <td width="5%">{{ $soli->idsolicitud  }}</td>
        <td width="5%">{{ $soli->idcliente  }}</td>
        <td width="15%">{{ $soli->idservicio }}</td>
        <td width="10%">{{ $soli->nservicio }}</td>
        <td width="25%">
        <center>
           <a class="dt-button buttons-pdf buttons-html5 btn blue btn-outline " href="/Solicitud/Editar/{{ $soli->idsolicitud }}" title="editar" id="buttonHola" ><i class='fa fa-edit'></i> </a>
           <a class="dt-button buttons-pdf buttons-html5 btn blue btn-outline  " data-toggle="modal"  title="Ver" href="#productover" id="buttonHola" ><i class='fa fa-eye'></i></a>
           <a  class="btn red btn-outline sbold derecha"  title="Eliminar" class="btn red btn-outline sbold"  href="/Solicitud/Eliminar/{{ $soli->idsolicitud }}"><i class=' fa fa-trash'></i></a>

        </center>
        </td>
      </tr>

      @endforeach


                   </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
        </div>
      <!--visualizar datos del serivicio -->
        <!--<div class="modal fade" id="asignaservicio" tabindex="-1" role="basic" aria-hidden="true">
              <div id="login-overlay" class="modal-dialog">
                <div class="modal-content">
                      <div class="modal-header" style="border-bottom: 4px solid #2e77bc; background-color: #fff; color: #111;">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                          <h2 class="modal-title" id="myModalLabel" style="font-size: 9pt;"><h4><i class="fa fa-eye"></i>&nbsp; ASIGNACION DEL SERVICIO.</h4></h2>
                      </div>
                  <div style="margin-top: 1px; background-color: #2e77bc; height: 1px; width: 100%;"></div>
                    <div class="modal-body">
                        <div class="editinplace row">
                      <div class='col-xs-6'><div class='well'>
                        <div style='color: #acacac; font-size: 9pt; text-align: center; padding: 0px; margin-bottom: 6px;'>Detalles del servicio</div>

                  </div>
              </div>
              <div class='col-xs-6'>
                <div class='well'>
                        <div style='color: #acacac; font-size: 9pt; text-align: center; padding: 0px; margin-bottom: 6px;'>Lista de empleados</div>
                        <div class="row">
                        
                        </div>
                            </div>
                        </div>
            </br>
            <div class='col-xs-6'>
                <div class='well'>
                    <div style='color: #acacac; font-size: 9pt; text-align: center; padding: 0px; margin-bottom: 6px;'>Lista de Vehiculos</div>
                    
                  </div>
              </div>
              <div class="button">
                                      <center>
                                       <button type="submit" class="btn btn-primary btn-lg" name="enviar" value="Guardar">Enviar</button></center>
                                    </div>
        </div>
        </div>
      -->
        <!--modal editar -->
        <!--<div class="modal fade" id="productoedita" tabindex="-1" role="basic" aria-hidden="true">
              <div id="login-overlay" class="modal-dialog">
                <div class="modal-content">
                      <div class="modal-header" style="border-bottom: 4px solid #2e77bc; background-color: #fff; color: #111;">
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                          <h2 class="modal-title" id="myModalLabel" style="font-size: 9pt;"><h4><i class="fa fa-edit"></i>&nbsp; EDICION DE SERVICIO.</h4></h2>
                      </div>
                  <div style="margin-top: 1px; background-color: #2e77bc; height: 1px; width: 100%;"></div>
                    <div class="modal-body">
                        <div class="editinplace2 row">

                        </div>
                    </div>
                  </div>
              </div>
        </div>-->

        <script LANGUAGE="JavaScript">
function confirmDel(url){
//var agree = confirm("¿Realmente desea eliminarlo?");
if (confirm("¿Realmente desea eliminar este Servicio?"))
    window.location.href = url;
else
    return false ;
}
</script>
    <br>
    <br>
    <br>


                          
                  <footer>
      <div class="footer-copy-redes" style="background-color:#2E86C1;">
        <div class="main-copy-redes">
          <div class="footer-copy">
            <center>   <div class="page-footer-inner"> <?php echo  $hoy2 = date("Y"); ?>
                <a  title="Contacta con +58 0426-8734726" target="_blank">SITIO OFICIAL DECSE TECHNOLOGY</a></center> 
          </div>
          <div class="footer-redes">
            <li><a href="https://www.twitter.com" class="" title="twitter">
            <img class="crafty-social-button-image"alt="twitter" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/twitter.png">
            </a>
            </li>
            <li><a href="https://www.facebook.com/decsetechnology" class="" title="facebook">
            <img class="crafty-social-button-image"alt="facebook" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/facebook.png">
            </a>
            </li>
            <li><a href="https://wwww.gmail.com" class="l" title="Email">
            <img class="crafty-social-button-image"alt="Email" width="44" height="44" src="https://www.ttandem.com/wp-content/plugins/crafty-social-buttons/buttons/simple/email.png">
            </a>
            </li>
          </div>
        </div>
      </div>
</div>
    </footer> 
    <!-- Footer -->
@endsection